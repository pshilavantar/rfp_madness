"""
Either setuptools or distribute must be installed in order for
this module to run.

For development work, run "python setup.py develop" from the
top level package directory where this setup.py module resides.
"""

from setuptools import find_packages, setup


setup(
    name='rfp_madness',
    version='0.1',
    #packages=['rfp_madness', 'rfp_madness.kbase', 'rfp_madness.testing_support', 'tests'],
    url='https://bitbucket.org/bradallen/rfp_madness',
    license='Tentative...possibly LGPL.',
    author='Brad Allen',
    author_email='bradallen137@gmail.com',
    description=('A Django-based web app (unreleased, still in development) for '
                'teams who find themselves responding to a lot of incoming RFPs, '
                'and need a better way to collaborate than taking turns editing '
                'a bunch of Word and Excel documents.'),
    packages=find_packages(exclude=["tests"]),
    zip_safe = False,
    include_package_data = True,
    entry_points = {
        'console_scripts': [
            'populate_demo_data = rfp_madness.scripts.populate_demo_data:main',
            ],
        },
    install_requires = ['django>=1.4.0',
                        'pysistence',
                        'unittest2', # for Python 2.5 and 2.6 compatibility
                        'ordereddict', # for Python 2.5 and 2.6 compatibility
                        'factory_boy'
                        ],
    tests_require = ['django-discover-runner',],
)
