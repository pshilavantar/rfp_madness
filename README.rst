Welcome to RFP Madness!!!
=========================

A Django-based web app for teams who find themselves responding to a lot of
incoming RFPs, and need a better way to collaborate than taking turns editing
a bunch of Word and Excel documents.

This project is still in development. Goals include:

    * Support a workflow around each RFP answer to a question, including
      versioned answers, reviews, editing, approval,  voting, and disputes.
    * Support uploading RFP documents and attaching metadata
    * Support decomposing uploaded documents into individual question
      records. (This is probably the most ambitious, UI-intensive feature
      planned and may take the most time). Until this is developed.
    * Support linking questions between different RFPs, with a "link type"
      to indicate the nature of the link ('identical question',
      'nearly identical question', 'similar question', etc.)
    * Support attaching "subject category" metadata to questions in order
      to suggest links.
    * Support manual assignment of a question or a section to a team or
      person ("SME"), with default assignments auto-populated based on
      selected subject category.
    * Support searching, of course! This would include full text search
      of questions, answers, discussion, and subject areas. In effect,
      previous answers to questions can act as a knowledgebase to be used
      for building new responses to RFP questions.

If it sounds a bit ambitious, hey...that's why it's titled "RFP Madness"!

Also the RFP process can be quite maddening, by distracting a team or a
whole company from actually developing their products, as they endeavor
to author essay questions to hundreds and possibly thousands of questions
on any given RFP.

So, this project aims to fight madness with madness, and lighten the load
of those SMEs who really get tired of answering the same questions over
and over in slightly different ways. In fact, RFP coordinators should be
able to find many answers from prior RFPs to compose new answers without
having to bother the SMEs except for a lightweight review.

Requirements
------------

RFP Madness requires Django 1.4 or greater, which in turn requires
a version for Python from 2.5 to 2.7.

In addition, either `setuptools` or `distribute` are required in order
to run the setup.py file.

Detailed dependencies are included as part of the metadata within
the setup.py file. To automatically install all dependencies, navigate
to the top level project directory containing the setup.py, and
run either of the following commands:

    * python setup.py install  (for purposes of installing)
    * python setup.py develop (for purposes of trying out or developing)


Configuration
-------------

The rfp_madness/settings.py file contains most of the documentation about
what needs to be configured for any particular install. This can be
found in the docstring at the top of the file.


To-Do
-----

* flesh out the UI
    -  forms for editing all model entities
    - form to upload actual RFP document blobs
    - implement UI to link questions to each other, and suggest possible matches
* set up demo site
* switch to py-bcrypt for better password encryption
* script to populate demo data (so manage.py reset doesn't keep wiping it out)
* transcribe user stories from paper into a document in this repo
* implement all user stories
* transcribe paper ERD onto ERD document in this repo
* Suggest to django-debug-toolbar folks that Django's DEBUG=False
  should also disable the debug toolbar.



Testing Tools
-------------

The following need to be installed in order to run tests

* django-discover-runner
* factory_boy # better way to create test fixtures for Django models

The following are planned:

* WebTest # better than Django built-in test client
* selenium # take advantage of Django's built-in Selenium support for web UI testing

The decision to use these tools was influenced by a
PyCon 2012 presentation by Carl Meyer titled "Testing and Django":

    http://pyvideo.org/video/699/testing-and-django

The 'django-discover-runner' was not mentioned during his presentation,
but was inspired directly by this same presentation.

    http://pypi.python.org/pypi/django-discover-runner

Here are the motivations to use django-discover-runner instead of
Django's built-in test runner.

    * Still allows use of some of Django's built-in testing features
      (as opposed to using nose or py.test, this still goes through
      running Django's "manage.py test")

    * Discover tests wherever you want them, removing constraint
      of having a single test module within each Django app package.

    * Does not run tests from external apps/frameworks by default (which
      Django's test runner does...which takes time and is unnecessary,
      since I don't need to run Django framework tests every time
      I want to run tests specific to my application.)

    * Flexible specification of specific tests to run: Python dotted
      path to test module, not Django app label (which is coarse grained),
      like this:

          ./manage.py test tests.test_models.test_rfp



About the Module Structure
--------------------------

The main app under development currently is in the rfp_madness.kbase subpackage,
whose purpose is to provide an interface to a knowledgebase and workflow engine
to respond to an RFP. Most knowledgebases support a workflow process, and 'kbase'
is a commonly recognized abbreviation of 'knowledgebase'.

This leaves an opening for other Django apps to be created as subpackages
which could address other aspects of the RFP process, such as creating an
RFP, generating RFP documents, collecting RFP artifacts, generating proposals,
and providing a way to generate nicely formatted final deliverables for
a proposal. However, none of those are in the short term plan for development.
Right now, the knowledgebase is the main goal.





