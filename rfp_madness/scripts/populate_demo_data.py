"""
Script to load the database with demo data.
"""

import rfp_madness.testing_support.model_factories as mf

#import sys
#try:
#    import rfp_madness.settings # needed to utilize transactions
#except ImportError:
#    print sys.path
#    sys.exit(1)
from django.db import transaction

import rfp_madness.kbase.models as kb



# ---------- Categories -------

@transaction.commit_on_success
def insert_prospect_rfps():
    prospect_rfps = (
      ('Society of Incongruous Anomalies','Request for Sightings'),
      ('Disinterested Corporation','Proposal for...Never Mind. Forget it.'),
      ('Incorporeal, Inc.','Call for Dimensional Transposition Proposals'),
      ('The Disinfotainment Totality','Seeking Enjoyable Assimilation'),
      ('ACME, Inc.','Calls for Proposals of a Better Birdtrap'),
        )
    for prospect_name, title in prospect_rfps:
        print prospect_name, title
        prospect = mf.ProspectFactory.create(prospect_name=prospect_name)
        rfp = mf.RfpFactory.create(title=title, prospect=prospect)
        prospect.save()
        rfp.save()


@transaction.commit_on_success
def insert_subject_categories():
    models = []
    subject_categories = ('Integration',
                          'Automation',
                          'Infrastructure',
                          'Dance Moves'
                          'Content',
                          'Business Requirements',
                          'Legal',
                          'Financial',
                          'Content')
    for subject_category in subject_categories:
        print subject_category
        model = kb.SubjectCategory(category_name=subject_category)
        model.save()



def main():
    insert_subject_categories()
    insert_prospect_rfps()