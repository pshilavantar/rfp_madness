"""
Tests for model classes can be not only tedious to write by hand
(due to interdependencies between multiple model classes), but
also can make tests run slower due to the need to sometimes
set up database fixtures and send actual SQL statements to the
database.

That's where 'factory_boy' models come into the picture.
They provide a nice re-usable abstraction for creating database
fixtures which can be specified whether or not they touch the
actual database.
"""

from datetime import datetime, date

import factory


import rfp_madness.kbase.models as models

class SubjectCategoryFactory(factory.Factory):
    FACTORY_FOR = models.SubjectCategory
    category_name = 'Unknown'


class ProspectFactory (factory.Factory):
    FACTORY_FOR = models.Prospect
    prospect_name = 'ACME, Inc.'
    contact_name = 'Wile E. Coyote'
    contact_email = 'wile.e.coyoyote@acme_inc.com'


class RfpFactory(factory.Factory):
    FACTORY_FOR = models.Rfp
    title = 'Calls for Proposals of a Better Birdtrap'
    prospect = ProspectFactory()
    request_dt = date(2012, 5, 1)
    next_due_dt = date(2012, 5, 15)
    delivery_due_dt = date(2012, 5, 30)

