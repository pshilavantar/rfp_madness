
import rfp_madness.kbase.models as kbmodels
from django.contrib import admin


class RfpDocumentInline (admin.TabularInline):
    """
    Provide an admin form widget for adding/editing an RFP document.
    """
    model = kbmodels.RfpDocument
    extra = 4

class RfpInline (admin.TabularInline):
    """
    Provide an admin form widget for adding/editing an RFP
    """
    model = kbmodels.Rfp
    extra = 1

class ProspectRfpAdmin(admin.ModelAdmin):
    """
    Provide an admin screen for an adding/editing a group of
    related entities:

        * Prospect
        * RFP
        * RFP documents
    """
    inlines = [RfpInline, ]


class RfpAdmin(admin.ModelAdmin):
    """
    Provide an admin screen for an adding/editing a group of
    related entities:

        * Prospect
        * RFP
        * RFP documents
    """
    inlines = [RfpDocumentInline, ]



admin.site.register(kbmodels.Prospect, ProspectRfpAdmin)
admin.site.register(kbmodels.Rfp, RfpAdmin)

admin.site.register((
                     # kbmodels.Prospect,
                     #kbmodels.Rfp,
                     kbmodels.RfpAnswer,
                     # kbmodels.RfpDocument,
                     kbmodels.Article,
                     kbmodels.RfpQuestion,
                     kbmodels.Team,
                     kbmodels.SubjectCategory,
                     kbmodels.UserProfile
    ))



