"""
Django model classes representing the database schema for "RFP Madness".

Special constant values are also included here which play a role in
the workflow.
"""

from django.db import models
from django.contrib.auth.models import User

ANSWER_STATUS_VALUES = (
    ('prp', 'Proposed Answer' ),
    ('chk', 'Accuracy Disputed'),
    ('edt', 'Edit for Language Correctness or Style'),
    ('rej', 'Rejected'),
    ('apr', 'Approved'),
    )

ARTICLE_STATUS_VALUES = (
    ('pnd', 'Answer is Pending'),
    ('res', 'Needs Research' ),
    ('dsc', 'Needs Discussion' ),
    ('rev', 'Ready for Review'),
    ('fin', 'Closed With Final Answer'),
    )


class RfpMadnessModel (models.Model):
    """
    Attributes common to all RFP Madness models.

    This model will not define an additional table, because it has been
    declared as 'abstract'.
    """
    class Meta:
        abstract = True
    create_dt = models.DateTimeField('Date Created', auto_now_add=True)
    update_dt = models.DateTimeField('Date Modified', auto_now=True)


class SubjectCategory (RfpMadnessModel):
    class Meta:
        verbose_name_plural = "Subject Categories"
        verbose_name = "Subject Category"
    category_name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.category_name


class Team (RfpMadnessModel):
    """Every user will be a member of a team."""
    team_name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.team_name


class UserProfile (RfpMadnessModel):
    class Meta:
        verbose_name_plural = "User Profiles"
        verbose_name = "User Profile"
    # This field is required by Django's authentication system
    user = models.OneToOneField(User)

    # Other fields here
    description = models.TextField()
    team = models.ForeignKey(Team)

    def __unicode__(self):
        return self.user.username


class Prospect (RfpMadnessModel):
    """
    Represents the organization which created an RFP and posed
    its myriad questions.
    """
    prospect_name = models.CharField(max_length=50)
    # TODO break out contacts into a separate table to allow multiples
    contact_name = models.CharField(max_length=50, blank=True)
    contact_email = models.CharField(max_length=50, blank=True)
    contact_phone = models.CharField(max_length=25, blank=True)

    def __unicode__(self):
        return self.prospect_name


class Rfp (RfpMadnessModel):
    """
    Represents a complete RFP.
    """
    class Meta:
        verbose_name_plural = 'RFPs'
        verbose_name = 'RFP'
    title = models.CharField(max_length=80)
    prospect = models.ForeignKey(Prospect)
    request_dt = models.DateField(verbose_name='Date Requested',
                                  help_text='When the RFP arrived.')
    next_due_dt = models.DateField(help_text="An internal due date, "
                                   "set for each stage of the workflow")
    delivery_due_dt = models.DateField(help_text="Final due date the RFP response"
                                                 " needs to ship out.")

    def __unicode__(self):
        return self.title


class RfpDocument (RfpMadnessModel):
    """
    Represents a single document from an RFP.
    """
    # TDDO support a FileField for uploading document blobs
    FILE_TYPES = (
        ('xls', 'MS Excel legacy format'),
        ('xlsx', 'MS Excel'),
        ('doc', 'MS Word legacy format'),
        ('docx', 'MS Word'),
        )
    rfp = models.ForeignKey(Rfp)
    doc_name = models.CharField(max_length=50)
    doc_type = models.CharField(max_length=4, choices=FILE_TYPES)

    def __unicode__(self):
        return unicode(self.rfp) + ' ' + unicode(self.doc_name)


class RfpQuestion (RfpMadnessModel):
    """
    Represents a single question from an RFP document.

    Any question may be linked to one or more other questions,
    through the `related_questions` attribute.
    """
    class Meta:
        verbose_name_plural = 'RFP Questions'
        verbose_name = 'RFP Question'
    question_text = models.TextField()
    rfp_doc = models.ForeignKey(RfpDocument)
    rfp_doc_section = models.CharField(max_length=50)
    related_questions = models.ManyToManyField('self', blank=True)
    categories = models.ManyToManyField(SubjectCategory)

    def __unicode__(self):
        #don't want full length of question to appear in admin screens
        return self.question_text[:40].strip()


class RfpAnswer (RfpMadnessModel):
    """
    Represents a possible answer for an RFP question.
    """
    class Meta:
        verbose_name_plural = 'RFP Answers'
        verbose_name = 'RFP Answer'
    answer_text = models.TextField()
    status = models.CharField(max_length=3, choices=ANSWER_STATUS_VALUES,
                              default='prp')
    author = models.ForeignKey(User, related_name='+')
    derived_from = models.ForeignKey('self', blank=True)
    article = models.ForeignKey('Article')
    approval_level = models.IntegerField(default=0)
    # TODO create a ManyToMany model to track who voted which way
    votes = models.IntegerField(default=0)

    def __unicode__(self):
        return self.author + ' ' + unicode(self.create_dt)


class Article (RfpMadnessModel):
    """
    Represents a knowledgebase article, which consists of an
    RFP question, answers, and commentary.
    """
    question = models.OneToOneField(RfpQuestion)
    current_answer = models.ForeignKey(RfpAnswer, related_name='article_parent')
    assigned_sme = models.ForeignKey(User, related_name='+')
    assigned_reviewer = models.ForeignKey(User, related_name='+', blank=True)
    assigned_editor = models.ForeignKey(User, related_name='+', blank=True)
    assigned_team = models.ForeignKey(Team, blank=True)
    status = models.CharField(max_length=3, choices=ARTICLE_STATUS_VALUES,
                              default='pnd')

    def __unicode__(self):
        return self.question.rfp_doc.rfp